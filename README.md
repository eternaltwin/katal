# Katal

Deployment tool for Eternaltwin.

Katal is a cross-platform build tool and package manager. The desired system state is described as a tree of idempotent
tasks, and then Katal updates the system to reach the desired state. The intent is to act as a better Ansible.

Linux is the primary target platform. Services are deployed using SystemD unit files.

## Usage

### Sync the current system

```bash
cargo run -- plan
cargo run -- sync
```

### Encrypt a secret

```bash
cargo run -- vault encrypt
```

The id is the dot-separated path inside the vault field. For example, to encrypt `{vault: {eternaltwin: {dev: {db_main_password: <value>}}}}`,
the id will be `eternaltwin.dev.db_main_password`.

Example:

```txt
$ cargo run -- vault encrypt
    Finished dev [unoptimized + debuginfo] target(s) in 0.25s
     Running `target/debug/katal vault encrypt`
Vault super-key?: [hidden]
Id for the value?: sudo_password
Value to encrypt?: [hidden]
$katal$v=1$$EVRl8gjoM4F1DuQi$0qAB3fIspSkdROYxpJYHKxvFQ8qw1mvdqIg=
```

### Refresh cached indexes

```
curl https://nodejs.org/download/release/index.json > ./crates/katal/files/node/index.json
cargo run -- fetch-index
```

## Configuration

Katal is pull-based system configured in two steps:
1. The config loader
2. The main config

The config loader is a minimal config describing how to retrieve the current system config. This means that a system
with katal configured can manually fetch the wanted config. The main benefit of such a system (as opposed to a system
where the config is pushed to the system) is that you don't need to provide external access to the system.

Once the main config is loaded, it is used to described the desired system state.

### Loader config

The loader config is defined in `katal.toml` in the repo root.

There are a couple loader types:

#### Directory loader 

```toml
[loader]
type = "Dir"
dir = "./config/"
```

Read the config from a local directory `dir`. It will read `.json` files and merge them all.
It means that if the config dir contains `./apps/eternaltwin.json` (1) and `./apps/dinocard.json` (2), the resolved
config will contain `{apps: {eternaltwin: <content1>, dinocard: <content2>}}`.

### Git loader

Like the directory loader, but load the config from a repo.

```toml
[loader]
type = "Git"
repo = "https://gitlab.com/eternaltwin/config"
dir = "norray"
ref = "main"
```

This will fetch the provided repo, checkout `ref`, and use the directory loader on `dir` inside the repo.

### Secrets

Katal needs access to secrets. Those are stored in the `vault` field of the main config. However, to unlock the vault
you need to configure the super-key in the loader.

```toml
[daemon]
key = "dev_vault_key_here"
```
