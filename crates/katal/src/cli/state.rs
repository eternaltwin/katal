use crate::config::{find_new_config, FindConfigError};
use clap::Parser;
use katal_state::sqlite::SqliteStore;

/// Arguments to the `state` command.
#[derive(Debug, Parser)]
pub struct Args {}

pub async fn run(_args: &Args) -> Result<(), anyhow::Error> {
  let path = std::env::current_dir().unwrap();
  let config = match find_new_config(path) {
    Ok(config) => Some(config),
    Err(FindConfigError::NotFound(_)) => None,
    Err(e) => return Err(anyhow::Error::from(e)),
  };
  let config = config.expect("missing config");
  SqliteStore::new(config.state.path, true).await;
  Ok(())
}
