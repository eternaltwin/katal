pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const GIT_REVISION: &str = env!("VERGEN_GIT_SHA");
