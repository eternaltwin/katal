use crate::pg_sql::PgRoleName;
use crate::ssh_keygen::SshKeyEd25519;
use crate::ssh_known_hosts::{
  append_known_hosts, keyscan, read_known_hosts, ssh_fingerprint, HostnamePattern, KeyTypes, KnownHostSpec, SshUrl,
};
use crate::task::bashrc::BashRc;
use crate::task::borg::InitBorgRepo;
use crate::task::borgbase::{
  GraphqlId, HttpBorgbaseClient, PublicSshKey, RepoAddArgs, RepoBorgVersion, RepoEditArgs, SshAddArgs, SshDeleteArgs,
};
use crate::task::borgmatic::BorgRepository;
// use crate::task::certbot::CertbotCert;
use crate::task::certbot::CertbotCert;
use crate::task::clickhouse::env::{
  ClickhouseDatabaseStatus, ClickhouseUserStatus, TargetClickhouseDatabase, TargetClickhouseDatabaseGroup,
  TargetClickhouseEnv, TargetClickhouseEnvError, TargetClickhousePermission, TargetClickhouseUser,
  TargetClickhouseUserGroup, TargetClickhouseUserState,
};
use crate::task::clickhouse::{ChUserName, ClickhouseVersion};
use crate::task::elasticsearch::ElascticsearchClusterTarget;
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureFile};
use crate::task::java::{JavaEnv, JavaReady, JavaReadyError};
use crate::task::nginx::{NginxEnv, NginxReady, NginxReadyError};
use crate::task::node::{GetNode, NodeIndex, NodeRelease};
use crate::task::nvm::{Nvm, NvmEnv};
use crate::task::php::{NamedPhpFpmPoolConfig, PhpAvailable, PhpFpmPoolConfig, PhpFpmSocketAddr, PhpFpmUnixGroup};
use crate::task::postgres::{
  PostgresCluster, PostgresDb, PostgresDbPermissions, PostgresRole, PostgresState, PostgresVersionReq,
};
use crate::task::rust::{RustEnv, Rustup};
use crate::task::systemd::restart_systemd_unit;
use crate::task::user::{Group, UpsertGroupByNameError, UpsertUserByNameError, User};
use crate::task::yarn::{YarnEnv, YarnReady, YarnReadyError};
use crate::task::{exec_as, AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::{DirEntry, FsHost, Metadata, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{GroupRef, LinuxGroup, Uid, UserRef};
use asys::linux::user::{LinuxUser, LinuxUserHost};
use asys::local_linux::LocalLinux;
use asys::ExecHost;
use chrono::Utc;
use katal_loader::tree::ReadFsx;
use semver::VersionReq;
use serde::{Deserialize, Serialize};
use std::cmp::max;
use std::collections::HashSet;
use std::fmt::Debug;
use std::path::{Path, PathBuf};
use tokio::time::Duration;
use url::Url;

pub const FALLBACK_INDEX: &str = include_str!("../../files/fallback/index.html");

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct NodeEnv {
  pub exe: PathBuf,
  pub bin_dir: PathBuf,
  pub release: NodeRelease,
}

impl NodeEnv {
  pub fn bin_dir(&self) -> &Path {
    self
      .exe
      .parent()
      .expect("retrieving `bin_dir` from `NodeEnv` always succeeds")
  }
}

/// A channel is a set of unique resources:
/// Domain name, Linux user, port, directory
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelTarget<S = String>
where
  S: AsRef<str>,
{
  /// Name of the channel (must be globally unique)
  /// Usually of the form `<app>.<channel>` such as `etwin.production` or `etwin.beta`.
  pub full_name: S,
  /// Short name of the channel (describes the environment)
  /// Examples: `production`, `staging`.
  pub short_name: S,
  /// Use to use for this channel.
  pub user: ChannelUserTarget<S>,
  /// Domains to use for this channel. None if no domains are needed.
  pub domain: Option<ChannelDomainTarget<S>>,
  /// Reserve a writable directory for the app's data
  pub data_dir: bool,
  /// Get a Postgres database environment
  pub postgres: Option<ChannelPostgresTarget<S>>,
  /// Get a Clickhouse database environment
  pub clickhouse: Option<ChannelClickhouseTarget<S>>,
  /// Get an Elasticsearch database environment
  pub elasticsearch: Option<ChannelElasticsearchTarget<S>>,
  /// Provide a PHP environment with PHP-FPM and Composer.
  pub php: Option<PhpAvailable>,
  /// Provide a specific Node version for build time
  pub node: Option<VersionReq>,
  /// Provide a local Cargo (Rust) environment
  pub rust: bool,
  /// Provide a local Java environment
  pub java: bool,
  /// Provide a local Yarn environment
  pub yarn: bool,
  /// Reserve the provided server port.
  pub main_port: Option<u16>,
  /// Generate a fallback server using the provided port.
  pub fallback_server: Option<u16>,
  /// Generate a borg repo with borgbase and borgmatic support.
  pub borg: Option<BorgTarget<S>>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelUserTarget<S: AsRef<str> = String> {
  /// Name of the user for this channel
  pub name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelDomainTarget<S: AsRef<str> = String> {
  /// Main domain name.
  pub main: S,
  /// Also handle the domain `www.<main>`.
  pub www: bool,
  /// List of legacy domains: acquire a certificate for them and add a redirect.
  pub legacy: Vec<S>,
  /// List of alternative domain names: acquire a certificate for them.
  pub alt: Vec<S>,
  /// Acquire certificate for the domains. None for no certificate.
  pub cert: Option<CertTarget<S>>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelPostgresTarget<S: AsRef<str> = String> {
  pub admin_password: S,
  pub main_password: S,
  pub read_password: S,
  pub reset: bool,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelClickhouseTarget<S: AsRef<str> = String> {
  pub sys_password: S,
  pub main_password: S,
  pub read_password: S,
  pub reset: bool,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelElasticsearchTarget<S: AsRef<str> = String> {
  pub password: S,
  pub port: u16,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct CertTarget<S: AsRef<str> = String> {
  /// Email address to use to register the certificate.
  pub email: S,
  /// Name of the certificate. Default: Channel name.
  pub name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BorgTarget<S: AsRef<str> = String> {
  /// API key to configure borgbase keys and repositories.
  pub borgbase_api_key: S,
  /// Password for the borg repository.
  pub repo_password: S,
}

impl ChannelTarget<String> {
  pub fn new<S: AsRef<str>>(full_name: S, short_name: S) -> Self {
    Self {
      full_name: full_name.as_ref().to_string(),
      short_name: short_name.as_ref().to_string(),
      user: ChannelUserTarget {
        name: full_name.as_ref().replace('.', "_"),
      },
      domain: None,
      data_dir: false,
      postgres: None,
      clickhouse: None,
      elasticsearch: None,
      java: false,
      php: None,
      node: None,
      rust: false,
      yarn: false,
      main_port: None,
      fallback_server: None,
      borg: None,
    }
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Channel<User: LinuxUser, S: AsRef<str> = String> {
  pub full_name: S,
  pub short_name: S,
  pub user: User,
  pub domain: Option<ChannelDomain<S>>,
  pub paths: ChannelPaths,
  pub main_port: Option<u16>,
  pub fallback_server: Option<ChannelFallbackServer>,
  pub postgres: Option<ChannelPostgres>,
  pub clickhouse: Option<ChannelClickhouse>,
  pub data_dir: Option<ChannelDataDir>,
  pub elasticsearch: Option<ChannelElasticsearch>,
  // pub nvm: Option<NvmEnv>,
  pub java: Option<JavaEnv>,
  pub nginx: NginxEnv,
  pub node: Option<NodeEnv>,
  pub php: Option<ChannelPhp>,
  pub rust: Option<RustEnv>,
  pub yarn: Option<YarnEnv>,
  pub borg: Option<ChannelBorgbase>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelDomain<S: AsRef<str> = String> {
  /// Main domain name.
  pub main: S,
  /// Has a `www.<main>`.
  pub www: bool,
  /// Legacy domains (keep redirections)
  pub legacy: Vec<S>,
  /// List of alternative domain names (for nginx server_name directive).
  pub alt: Vec<S>,
  /// HTTPS certificate for the domains.
  pub cert: Option<ResolvedHttps>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelPaths {
  /// Main channel directory, contains all the other directories.
  pub home: PathBuf,
  /// Readonly directory of commits.
  pub commits: PathBuf,
  pub releases: PathBuf,
  pub logs: PathBuf,
  /// Symbolic link to the active release (may be missing)
  pub active_release_link: PathBuf,
  /// Symbolic link to the previous active release (may be missing)
  pub previous_release_link: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelFallbackServer {
  pub port: u16,
  pub dir: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelPostgres {
  pub cluster: PostgresState,
  pub host: String,
  pub port: u16,
  pub name: String,
  pub admin: PostgresCredentials,
  pub main: PostgresCredentials,
  pub read: PostgresCredentials,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelClickhouse {
  pub host: String,
  pub http_port: u16,
  pub ch_tcp_port: u16,
  pub name: String,
  pub main: ClickhouseCredentials,
  pub read: ClickhouseCredentials,
}

/// Writable directory for this channel
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelDataDir {
  path: PathBuf,
}

impl ChannelDataDir {
  pub fn path_buf(&self) -> PathBuf {
    self.path.clone()
  }

  pub fn file_uri(&self) -> Url {
    Url::from_directory_path(&self.path).expect("ChannelDataDir.path should allow to build a valid Url")
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelElasticsearch {
  pub uri: Url,
  pub port: u16,
  pub service: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct PostgresCredentials {
  pub name: PgRoleName,
  pub password: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ClickhouseCredentials {
  pub name: ChUserName,
  pub password: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelBorgbase {
  pub ssh_pub_key: PathBuf,
  pub ssh_priv_key: PathBuf,
  pub repo: BorgRepository,
  pub password: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelPhp {
  pub socket: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ResolvedHttps {
  pub cert_name: String,
  pub cert_path: PathBuf,
  pub fullchain_cert_path: PathBuf,
  pub cert_key_path: PathBuf,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum ChannelTargetError {
  #[error("failed to upsert web group")]
  GroupWeb(#[source] UpsertGroupByNameError),
  #[error("failed to upsert channel group")]
  Group(#[source] UpsertGroupByNameError),
  #[error("failed to upsert http user")]
  UserHttp(#[source] UpsertUserByNameError),
  #[error("failed to upsert channel user")]
  User(#[source] UpsertUserByNameError),
  #[error("failed to ensure /var/www directory")]
  EnsureVarWww(#[source] EnsureDirError),
  #[error("failed to ensure channel directory")]
  EnsureChannelDir(#[source] EnsureDirError),
  #[error("failed to prepare Java environment")]
  Java(#[source] JavaReadyError),
  #[error("failed to prepare Yarn environment")]
  Yarn(#[source] YarnReadyError),
  #[error("failed to prepare Nginx environment")]
  Nginx(#[source] NginxReadyError),
  #[error("failed to ensure channel data directory")]
  EnsureDataDir(#[source] EnsureDirError),
  #[error("failed to prepare Clickhouse environment")]
  Clickhouse(#[source] CreateClickhouseEnvError),
  #[error("other error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for ChannelTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.to_string())
  }
}

impl<S> ChannelTarget<S>
where
  S: AsRef<str>,
{
  pub fn paths(&self) -> ChannelPaths {
    let user_name = self.user.name.as_ref();
    let home = PathBuf::from("/var/www").join(user_name);
    let commits = home.join("commits");
    let releases = home.join("releases");
    let logs = home.join("logs");
    let active_release_link = home.join("active");
    let previous_release_link = home.join("previous");
    ChannelPaths {
      home,
      commits,
      releases,
      logs,
      active_release_link,
      previous_release_link,
    }
  }
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for ChannelTarget<S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
  S: AsRef<str> + Clone + Send + Sync,
{
  type Output = TaskResult<Channel<H::User, S>, ChannelTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    const WEB_GROUP: &str = "web";

    let mut changed = false;
    eprintln!("Create group: {}", WEB_GROUP);
    let res = Group::upsert_by_name(WEB_GROUP)
      .run(host)
      .await
      .map_err(ChannelTargetError::GroupWeb)?;
    changed = res.changed || changed;
    let _web_group: H::Group = res.output;
    let user_name = self.user.name.as_ref();
    eprintln!("Create group: {}", user_name);
    let res = Group::upsert_by_name(user_name)
      .run(host)
      .await
      .map_err(ChannelTargetError::Group)?;
    changed = res.changed || changed;
    let group: H::Group = res.output;
    let gid = group.gid();
    let paths: ChannelPaths = self.paths();
    eprintln!("Add group {} to Nginx worker (http) groups", user_name);
    let res = User::upsert_by_name("http")
      .add_secondary_group(user_name)
      .run(host)
      .await
      .map_err(ChannelTargetError::UserHttp)?;
    changed = res.changed || changed;
    eprintln!("Create user: {}", user_name);
    let res = User::upsert_by_name(user_name)
      .comment(&self.full_name)
      .group(GroupRef::Id(gid))
      .clear_secondary_groups()
      // .add_secondary_group(web_group.name())
      .shell("/bin/bash")
      .system(true)
      .home(&paths.home)
      .run(host)
      .await
      .map_err(ChannelTargetError::User)?;
    let user: H::User = res.output;
    changed = res.changed || changed;
    eprintln!("Create directory: /var/www");
    changed = EnsureDir::new(PathBuf::from("/var/www"))
      .owner(UserRef::ROOT)
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(ChannelTargetError::EnsureVarWww)?
      .changed
      || changed;

    for d in [&paths.home, &paths.logs, &paths.commits, &paths.releases] {
      eprintln!("Create directory: {}", d.display());
      changed = EnsureDir::new(d.clone())
        .owner(UserRef::Id(user.uid()))
        .group(GroupRef::Id(user.gid()))
        .mode(FileMode::OWNER_ALL | FileMode::GROUP_READ | FileMode::GROUP_EXEC)
        .run(host)
        .await
        .map_err(ChannelTargetError::EnsureChannelDir)?
        .changed
        || changed;
    }

    eprintln!("Prune old releases");
    prune_releases(host, &paths).await?;

    let channel_name = self.full_name.as_ref();

    // let nvm = if self.nvm {
    //   eprintln!("Create NVM environment");
    //   let res = create_nvm(&user, host).await?;
    //   changed = res.changed || changed;
    //   Some(res.output)
    // } else {
    //   None
    // };

    let node = if let Some(node) = self.node.as_ref() {
      eprintln!("Resolve Node: {node}");
      let res = create_node(node, host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let rust = if self.rust {
      eprintln!("Resolve Rust");
      let res = Rustup::new(user.uid()).run(host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let java = if self.java {
      eprintln!("Resolve Java");
      let res = JavaReady::new().run(host).await.map_err(ChannelTargetError::Java)?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let yarn: Option<YarnEnv> = if self.yarn {
      eprintln!("Resolve Yarn");
      let res = YarnReady::new(String::from("1.22.22"))
        .run(host)
        .await
        .map_err(ChannelTargetError::Yarn)?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    eprintln!("Resolve nginx");
    let nginx: NginxEnv = {
      let res = NginxReady::new().run(host).await.map_err(ChannelTargetError::Nginx)?;
      changed = res.changed || changed;
      res.output
    };

    let data_dir = if self.data_dir {
      let path = paths.home.join("data");
      eprintln!("Create directory: {}", path.display());
      changed = EnsureDir::new(path.clone())
        .owner(UserRef::Id(user.uid()))
        .group(GroupRef::Id(user.gid()))
        .mode(FileMode::OWNER_ALL)
        .run(host)
        .await
        .map_err(ChannelTargetError::EnsureDataDir)?
        .changed
        || changed;
      Some(ChannelDataDir { path })
    } else {
      None
    };

    let postgres = if let Some(postgres) = self.postgres.as_ref() {
      eprintln!("Create Postgres environment");
      let res = create_postgres_env(channel_name, postgres, host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let clickhouse = if let Some(clickhouse) = self.clickhouse.as_ref() {
      eprintln!("Create Clickhouse environment");
      let res = create_clickhouse_env(channel_name, clickhouse, host)
        .await
        .map_err(ChannelTargetError::Clickhouse)?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let elasticsearch = if let Some(elasticsearch) = self.elasticsearch.as_ref() {
      eprintln!("Create Elasticsearch environment");
      let res = create_elasticsearch_env(channel_name, elasticsearch, host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let domain = if let Some(domain) = self.domain.as_ref() {
      eprintln!("Configure domain names");
      let res = domain.run(host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let fallback_server = if let Some(fallback_port) = self.fallback_server {
      eprintln!("Create fallback server");
      let res = create_fallback_server(&user, fallback_port, host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let borg = if let Some(borg_target) = self.borg.as_ref() {
      eprintln!("Create Borg environment");
      let res = create_borgbase(self.full_name.as_ref(), &user, borg_target, host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let php = if let Some(php) = self.php.as_ref() {
      eprintln!("Create PHP environment");
      let res = create_php(self.full_name.as_ref(), user_name, user_name, php, host).await?;
      changed = res.changed || changed;
      Some(res.output)
    } else {
      None
    };

    let output = Channel {
      full_name: self.full_name.clone(),
      short_name: self.short_name.clone(),
      user,
      domain,
      paths,
      main_port: self.main_port,
      fallback_server,
      data_dir,
      postgres,
      clickhouse,
      elasticsearch,
      java,
      // nvm,
      nginx,
      node,
      php,
      rust,
      yarn,
      borg,
    };
    Ok(TaskSuccess { changed, output })
  }
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for ChannelDomainTarget<S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  S: AsRef<str> + Clone + Send + Sync,
{
  type Output = TaskResult<ChannelDomain<S>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    let cert: Option<ResolvedHttps> = if let Some(cert_target) = self.cert.as_ref() {
      let cert_name = cert_target.name.as_ref().to_string();
      let mut certbot_cert = CertbotCert::new(cert_target.email.as_ref(), cert_name.as_str(), self.main.as_ref());
      if self.www {
        certbot_cert = certbot_cert.domain(format!("www.{}", self.main.as_ref()));
      }
      for legacy in self.legacy.iter() {
        certbot_cert = certbot_cert.domain(legacy.as_ref());
      }
      for alt in self.alt.iter() {
        certbot_cert = certbot_cert.domain(alt.as_ref());
      }
      let cert_key_path = certbot_cert.get_priv();
      let fullchain_cert_path = certbot_cert.get_fullchain();
      let cert_path = {
        eprintln!("Acquire certificate");
        let res = certbot_cert.run(host).await?;
        changed = res.changed || changed;
        res.output
      };
      Some(ResolvedHttps {
        cert_name,
        cert_key_path,
        fullchain_cert_path,
        cert_path,
      })
    } else {
      None
    };
    let output = ChannelDomain {
      main: self.main.clone(),
      www: self.www,
      legacy: self.legacy.clone(),
      alt: self.alt.clone(),
      cert,
    };
    Ok(TaskSuccess { changed, output })
  }
}

async fn create_fallback_server<'h, H>(
  user: &impl LinuxUser,
  port: u16,
  host: &H,
) -> TaskResult<ChannelFallbackServer, anyhow::Error>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  let dir = user.home().join("fallback");

  let mut changed = false;
  changed = EnsureDir::new(PathBuf::from(&dir))
    .owner(UserRef::Id(user.uid()))
    .group(GroupRef::Id(user.gid()))
    .mode(FileMode::OWNER_ALL | FileMode::GROUP_READ | FileMode::GROUP_EXEC)
    .run(host)
    .await?
    .changed
    || changed;
  changed = EnsureFile::new(dir.join("index.html"))
    .content(FALLBACK_INDEX)
    .owner(user.uid())
    .group(user.gid())
    .mode(FileMode::OWNER_READ | FileMode::GROUP_READ)
    .run(host)
    .await?
    .changed
    || changed;

  let output = ChannelFallbackServer { port, dir };

  Ok(TaskSuccess { changed, output })
}

#[allow(unused)]
async fn create_nvm<'h, H>(user: &impl LinuxUser, host: &H) -> TaskResult<NvmEnv, anyhow::Error>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  let mut changed: bool = false;
  eprintln!("Initialize .nvm");
  let res = Nvm::new(user.uid()).path(user.home().join(".nvm")).run(host).await?;
  changed = res.changed || changed;
  let nvm_env = res.output;

  eprintln!("Create .bashrc");
  changed = BashRc::for_user(user).run(host).await?.changed || changed;

  eprintln!("Create .profile");
  let dot_profile = "export NVM_DIR=~/.nvm\n";
  changed = EnsureFile::new(user.home().join(".profile"))
    .content(dot_profile)
    .owner(user.uid())
    .group(user.gid())
    .mode(FileMode::OWNER_READ)
    .run(host)
    .await?
    .changed
    || changed;

  Ok(TaskSuccess {
    changed,
    output: nvm_env,
  })
}

async fn create_node<'h, H>(node_req: &VersionReq, host: &H) -> TaskResult<NodeEnv, anyhow::Error>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  let mut changed: bool = false;
  eprintln!("Get Node index");
  let index = NodeIndex::embedded();

  eprintln!("Resolve Node version");
  let release = index.resolve(node_req);
  let release: &NodeRelease = match release {
    Some(r) => r,
    None => return Err(anyhow::Error::msg("no compatible version found")),
  };

  eprintln!("Resolved Node version: {}", &release.version);
  let target_dir: PathBuf = PathBuf::from("/opt/node");
  let target_dir = target_dir.join(release.version.to_string());
  let ancestors = target_dir.ancestors().collect::<Vec<_>>();
  for dir in ancestors.into_iter().rev() {
    eprintln!("Create directory: {}", dir.display());
    changed = EnsureDir::new(dir.to_path_buf()).run(host).await?.changed || changed;
  }
  let target_path = target_dir.join("node");
  match host.read_file(target_path.as_path()) {
    Ok(_) => {
      eprintln!("Node already exists");
      // Already exists
      Ok(TaskSuccess {
        changed,
        output: NodeEnv {
          release: release.clone(),
          bin_dir: target_dir,
          exe: target_path,
        },
      })
    }
    Err(ReadFileError::NotFound) => {
      eprintln!("Node is missing, downloading");
      // Missing, install
      let node_dist = GetNode::release(release.clone()).run(host).await?.output;
      eprintln!("Downloaded node, extracting binary");
      let node_bin = node_dist.read(["bin".to_string(), "node".to_string()]).await?;
      eprintln!("Extracted node, writing to file system");

      changed = EnsureFile::new(target_path.clone())
        .content(node_bin)
        .owner(Uid::ROOT)
        .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
        .run(host)
        .await
        .unwrap()
        .changed
        || changed;
      eprintln!("Node installed successfully");

      Ok(TaskSuccess {
        changed,
        output: NodeEnv {
          release: release.clone(),
          bin_dir: target_dir,
          exe: target_path,
        },
      })
    }
    Err(e) => Err(e.into()),
  }
}

async fn create_postgres_env<'h, H, S>(
  db_name: &str,
  target: &ChannelPostgresTarget<S>,
  host: &H,
) -> TaskResult<ChannelPostgres, anyhow::Error>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
  S: AsRef<str>,
{
  let admin_credentials = PostgresCredentials {
    name: PgRoleName::new(format!("{}.admin", db_name)),
    password: target.admin_password.as_ref().to_string(),
  };
  let main_credentials = PostgresCredentials {
    name: PgRoleName::new(format!("{}.main", db_name)),
    password: target.main_password.as_ref().to_string(),
  };
  let read_credentials = PostgresCredentials {
    name: PgRoleName::new(format!("{}.read", db_name)),
    password: target.read_password.as_ref().to_string(),
  };

  let mut changed: bool = false;

  eprintln!("Create Postgres cluster");
  let pg_cluster = PostgresCluster::new(
    PathBuf::from("/var/lib/postgres/data17"),
    PostgresVersionReq::Version(String::from("17.2")),
  )
  .service_name(Some(String::from("postgresql17")))
  .run(host)
  .await?;
  changed = pg_cluster.changed || changed;

  eprintln!("Create admin Postgres user");
  changed = PostgresRole::<&str>::new(admin_credentials.name.as_ref())
    .superuser(true)
    .can_login(true)
    .md5_password(&admin_credentials.password)
    .run(host)
    .await?
    .changed
    || changed;

  eprintln!("Create main Postgres user");
  changed = PostgresRole::new(main_credentials.name.as_ref())
    .superuser(false)
    .can_login(true)
    .md5_password(&main_credentials.password)
    .run(host)
    .await?
    .changed
    || changed;

  eprintln!("Create read Postgres user");
  changed = PostgresRole::new(read_credentials.name.as_ref())
    .superuser(false)
    .can_login(true)
    .md5_password(&read_credentials.password)
    .run(host)
    .await?
    .changed
    || changed;

  eprintln!("Create Postgres database"); // TODO: Run `ALTER DATABASE db_name REFRESH COLLATION VERSION;` as needed (when the collation is updated)
  let mut task = PostgresDb::new(db_name)
    .owner(admin_credentials.name.clone())
    .encoding("UTF8")
    .locale("en_US.UTF-8");
  if target.reset {
    task = task.force_empty_schema();
  }
  changed = task.run(host).await?.changed || changed;

  eprintln!("Configure Postgres permissions");
  changed = PostgresDbPermissions::new(
    db_name,
    admin_credentials.name.clone(),
    main_credentials.name.clone(),
    read_credentials.name.clone(),
  )
  .run(host)
  .await?
  .changed
    || changed;

  let output = ChannelPostgres {
    cluster: pg_cluster.output,
    host: "localhost".to_string(),
    port: 5432,
    name: db_name.to_string(),
    admin: admin_credentials,
    main: main_credentials,
    read: read_credentials,
  };

  Ok(TaskSuccess { changed, output })
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum CreateClickhouseEnvError {
  #[error("failed to initialize clickhouse cluster")]
  ClickhouseCluster(#[from] TargetClickhouseEnvError),
}

async fn create_clickhouse_env<'h, H, S>(
  db_name: &str,
  target: &ChannelClickhouseTarget<S>,
  host: &H,
) -> TaskResult<ChannelClickhouse, CreateClickhouseEnvError>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  S: AsRef<str>,
{
  let main_credentials = ClickhouseCredentials {
    name: ChUserName::new(format!("{}.main", db_name)),
    password: target.main_password.as_ref().to_string(),
  };
  let read_credentials = ClickhouseCredentials {
    name: ChUserName::new(format!("{}.read", db_name)),
    password: target.read_password.as_ref().to_string(),
  };

  eprintln!("Create Clickhouse environment");

  let clickhouse_target = TargetClickhouseEnv {
    version: ClickhouseVersion::new(24, 11, 1, 2557),
    user_group: TargetClickhouseUserGroup {
      sys_password: target.sys_password.as_ref().to_string(),
      users: vec![
        TargetClickhouseUser {
          name: main_credentials.name.clone(),
          status: ClickhouseUserStatus::Present(Some(TargetClickhouseUserState {
            password: Some(main_credentials.password.clone()),
            permissions: Some(vec![TargetClickhousePermission {
              database: db_name.to_string(),
              write: true,
            }]),
          })),
        },
        TargetClickhouseUser {
          name: read_credentials.name.clone(),
          status: ClickhouseUserStatus::Present(Some(TargetClickhouseUserState {
            password: Some(read_credentials.password.clone()),
            permissions: None,
          })),
        },
      ],
      unmatched: ClickhouseUserStatus::Present(None),
    },
    database_group: TargetClickhouseDatabaseGroup {
      databases: vec![TargetClickhouseDatabase {
        name: db_name.to_string(),
        status: ClickhouseDatabaseStatus::Present,
      }],
      unmatched: ClickhouseDatabaseStatus::Present,
    },
  };
  let clickhouse = clickhouse_target.run(host).await?;
  let changed = clickhouse.changed;
  let clickhouse = clickhouse.output;

  let output = ChannelClickhouse {
    host: "::1".to_string(),
    http_port: clickhouse.http_port,
    ch_tcp_port: clickhouse.ch_tcp_port,
    name: db_name.to_string(),
    main: main_credentials,
    read: read_credentials,
  };

  Ok(TaskSuccess { changed, output })
}

async fn create_elasticsearch_env<'h, H, S>(
  db_name: &str,
  target: &ChannelElasticsearchTarget<S>,
  host: &H,
) -> TaskResult<ChannelElasticsearch, anyhow::Error>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str>,
{
  let esct = ElascticsearchClusterTarget {
    name: db_name.to_string(),
    port: target.port,
  };
  let res = esct.run(host).await?;
  let mut uri = Url::parse("http://localhost").unwrap();
  uri
    .set_port(Some(res.output.port))
    .expect("failed to set Elasticsearch port");
  let output = ChannelElasticsearch {
    uri,
    port: res.output.port,
    service: res.output.service,
  };
  Ok(TaskSuccess {
    changed: res.changed,
    output,
  })
}

async fn create_borgbase<'h, H, S>(
  channel_name: &str,
  user: &impl LinuxUser,
  target: &BorgTarget<S>,
  host: &H,
) -> TaskResult<ChannelBorgbase, anyhow::Error>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  S: AsRef<str>,
{
  let mut changed: bool = false;
  let ssh_dir = user.home().join(".ssh");
  eprintln!("Create directory: {}", ssh_dir.display());
  changed = EnsureDir::new(ssh_dir.clone())
    .owner(UserRef::Id(user.uid()))
    .group(GroupRef::Id(user.gid()))
    .mode(FileMode::OWNER_ALL)
    .run(host)
    .await?
    .changed
    || changed;

  eprintln!("Generate borg key");
  let ssh_key_task = SshKeyEd25519 {
    key: ssh_dir.join("borgbase"),
    comment: format!("Borgbase key for {}", channel_name),
    password: None,
  };
  {
    changed = ssh_key_task.run(host).await?.changed || changed;
    EnsureFile::new(ssh_key_task.priv_key_path())
      .owner(user.uid())
      .group(user.gid())
      .mode(FileMode::OWNER_READ)
      .run(host)
      .await?;
    EnsureFile::new(ssh_key_task.pub_key_path())
      .owner(user.uid())
      .group(user.gid())
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?;
  }

  let borgbase_client = HttpBorgbaseClient::new()?.with_auth(target.borgbase_api_key.as_ref().to_string());

  eprintln!("Register borg key with borgbase");
  let borgbase_key = {
    let mut keys = borgbase_client.get_ssh_keys().await?;
    let pub_key = std::fs::read_to_string(ssh_key_task.pub_key_path())?;

    keys.sort_by(|l, r| match (&l.added_at, &r.added_at) {
      (Some(l), Some(r)) => l.cmp(r),
      _ => l.id.cmp(&r.id),
    });

    let mut active_key: Option<PublicSshKey> = None;
    let mut keys_to_prune: Vec<GraphqlId> = Vec::new();
    for key in keys.into_iter() {
      if key.name != channel_name {
        continue;
      }
      if !pub_key.starts_with(&key.key_data) {
        keys_to_prune.push(key.id);
        continue;
      }
      let old_key = active_key.replace(key);
      if let Some(old_key) = old_key {
        keys_to_prune.push(old_key.id);
      }
    }
    for id in keys_to_prune.into_iter() {
      borgbase_client.delete_ssh_key(SshDeleteArgs { id }).await?;
    }

    match active_key {
      Some(ak) => ak,
      None => {
        borgbase_client
          .add_ssh_key(SshAddArgs {
            name: channel_name.to_string(),
            key_data: pub_key,
          })
          .await?
      }
    }
  };

  eprintln!("Acquire borg repository");
  let borgbase_repo = {
    let repos = borgbase_client.get_repos(Some(channel_name.to_string())).await?;
    let repo = repos.into_iter().next();
    let repo = match repo {
      Some(r) => {
        assert_eq!(r.name, channel_name);
        let key_str = borgbase_key.id.clone().inner();
        let new_keys = match r.full_access_keys.as_ref() {
          None => None,
          Some(keys) if keys.contains(&key_str) => None,
          Some(keys) => {
            let mut new_keys = keys.clone();
            new_keys.push(key_str);
            Some(new_keys)
          }
        };
        if let Some(new_keys) = new_keys {
          borgbase_client
            .edit_repo(RepoEditArgs {
              id: r.id,
              name: r.name,
              region: r.region,
              borg_version: r.borg_version,
              full_access_keys: Some(new_keys),
              append_only_keys: r.append_only_keys,
              rsync_keys: r.rsync_keys,
              quota: r.quota,
              quota_enabled: r.quota_enabled,
              alert_days: r.alert_days,
            })
            .await?
        } else {
          r
        }
      }
      None => {
        let new_repo = borgbase_client
          .add_repo(RepoAddArgs {
            name: channel_name.to_string(),
            region: "eu".to_string(),
            borg_version: RepoBorgVersion::Latest,
            full_access_keys: Some(vec![borgbase_key.id.clone().inner()]),
            append_only_keys: Some(Vec::new()),
            rsync_keys: Some(Vec::new()),
            quota: 2048,
            quota_enabled: true,
            alert_days: 7,
          })
          .await?;
        eprintln!("New Borgbase repository created: sleeping for 1 minute to allow for DNS propagation");
        tokio::time::sleep(Duration::from_secs(60)).await;
        new_repo
      }
    };
    repo
  };

  eprintln!("Register borgbase SSH public key");
  {
    let repo_path: SshUrl = borgbase_repo.repo_path.as_ref().unwrap().parse()?;
    let repo_fingerprint: &str = &borgbase_repo.server.as_ref().unwrap().fingerprint_ed25519;
    let repo_host: &str = &repo_path.host;
    let repo_host_path = HostnamePattern(repo_host.to_string());
    let known_hosts_path = ssh_dir.join("known_hosts");
    let cur_known_hosts = read_known_hosts(&known_hosts_path)?;
    let mut is_known = false;
    for cur_kh in cur_known_hosts.iter() {
      if let KnownHostSpec::Patterns(pats) = &cur_kh.spec {
        if pats.contains(&repo_host_path) {
          let fingerprint = ssh_fingerprint(host, &cur_kh.key_type, &cur_kh.pub_key)?;
          if fingerprint == repo_fingerprint {
            is_known = true;
          }
        }
      }
    }
    if !is_known {
      let ssh_host_entries = keyscan(host, KeyTypes::ed25519(), repo_host)?;
      for ssh_host_entry in ssh_host_entries {
        let fingerprint = ssh_fingerprint(host, &ssh_host_entry.key_type, &ssh_host_entry.pub_key)?;
        if fingerprint == repo_fingerprint {
          append_known_hosts(&known_hosts_path, &ssh_host_entry).await?;
          is_known = true;
          break;
        }
      }
    }
    if !is_known {
      return Err(anyhow::Error::msg("FaileToRegisterBorgbaseSshPublicKey"));
    }
    EnsureFile::new(known_hosts_path)
      .owner(user.uid())
      .group(user.gid())
      .mode(FileMode::OWNER_READ)
      .run(host)
      .await?;
  }

  let repo_path = borgbase_repo.repo_path.clone().unwrap();

  eprintln!("Initialize remote repo");
  if borgbase_repo.encryption == "none" {
    // TODO: Wait for DNS update, check with `systemd-resolve --flush-caches` (currently we sleep on repo creation)
    let init_repo = InitBorgRepo {
      password: target.repo_password.as_ref().to_string(),
      priv_key: ssh_key_task.priv_key_path(),
      repo_path: BorgRepository(repo_path.clone()),
    };
    exec_as::<LocalLinux, _>(UserRef::Id(user.uid()), None, &init_repo)
      .await?
      .unwrap();
  }

  let output = ChannelBorgbase {
    ssh_pub_key: ssh_key_task.pub_key_path(),
    ssh_priv_key: ssh_key_task.priv_key_path(),
    repo: BorgRepository(repo_path),
    password: target.repo_password.as_ref().to_string(),
  };

  Ok(TaskSuccess { changed, output })
}

async fn create_php<'h, H>(
  name: &str,
  user_name: &str,
  group_name: &str,
  target: &PhpAvailable,
  host: &H,
) -> TaskResult<ChannelPhp, anyhow::Error>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::Group: Send + Sync,
  H::User: Send + Sync + Debug,
{
  const NGINX_WORKER_USER: &str = "http";
  const NGINX_WORKER_GROUP: &str = "http";

  let mut changed: bool = false;
  eprintln!("Initialize PHP");
  let res = target.run(host).await?;
  changed = res.changed || changed;

  let socket = PathBuf::from("/run/php-fpm").join(format!("{}.sock", name));
  let config = NamedPhpFpmPoolConfig {
    name: name.to_string(),
    config: PhpFpmPoolConfig {
      user: user_name.to_string(),
      group: PhpFpmUnixGroup::Named(group_name.to_string()),
      listen: PhpFpmSocketAddr::Unix(socket.clone()),
      listen_owner: Some(NGINX_WORKER_USER.to_string()),
      listen_group: Some(NGINX_WORKER_GROUP.to_string()),
      pm: Some("dynamic".to_string()),
      pm_max_children: Some(33),
      pm_start_servers: Some(8),
      pm_min_spare_servers: Some(8),
      pm_max_spare_servers: Some(24),
      php_log_error: Some(format!("/var/www/{}/logs/fpm.error.log", name)),
    },
  };
  let pool_config_path = PathBuf::from("/etc/php/php-fpm.d").join(format!("{}.conf", name));
  EnsureFile::new(pool_config_path)
    .content(config.to_string())
    .owner(Uid::ROOT)
    .mode(FileMode::ALL_READ)
    .run(host)
    .await?;

  restart_systemd_unit("php-fpm.service", host)?;

  Ok(TaskSuccess {
    changed,
    output: ChannelPhp { socket },
  })
}

async fn prune_releases<'h, H>(host: &H, channel_paths: &ChannelPaths) -> TaskResult<(), anyhow::Error>
where
  H: LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  // Git refs that are unprotected and older than 1 week
  let mut releases_to_delete: HashSet<String> = HashSet::new();

  let release_dir_entries = host.read_dir(channel_paths.releases.as_path())?;

  // Git refs for the `active` and `previous` releases
  let mut protected_releases: HashSet<String> = HashSet::with_capacity(2);

  // It is important that we resolve link targets only after the release dir snapshot `release_dir_entries`
  // so it is more resilient in the face of concurrent updates.
  for protected_release in [&channel_paths.active_release_link, &channel_paths.previous_release_link] {
    let link_target = match host.resolve(protected_release.as_path()) {
      Ok(m) => m,
      Err(ResolveError::NotFound) => continue,
      Err(e) => return Err(e.into()),
    };
    let protected_ref = link_target
      .file_name()
      .expect("MissingTargetName")
      .to_str()
      .expect("CorruptedReleaseName")
      .to_string();
    protected_releases.insert(protected_ref);
  }

  let now = Utc::now();
  let one_week_ago = now - chrono::Duration::weeks(1);
  let one_week_ago = one_week_ago.timestamp();

  for release_dir_entry in release_dir_entries {
    let release_dir_entry = release_dir_entry.path();
    let entry_name = release_dir_entry
      .file_name()
      .expect("MissingEntryName")
      .to_str()
      .expect("CorruptedReleaseName")
      .to_string();
    if protected_releases.contains(&entry_name) {
      continue;
    }
    let meta: <H as FsHost>::Metadata = host.metadata(release_dir_entry.as_path())?;
    let latest_time = max(max(meta.creation_time(), meta.access_time()), meta.modification_time());
    if latest_time >= one_week_ago {
      continue;
    }
    releases_to_delete.insert(entry_name);
  }

  let mut changed = false;
  for release in releases_to_delete.into_iter() {
    eprintln!("Pruning old release {}", release);
    let release_dir = channel_paths.releases.join(release);
    std::fs::remove_dir_all(release_dir)?;
    changed = true;
  }

  Ok(TaskSuccess { changed, output: () })
}
