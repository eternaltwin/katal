use crate::discord_webhook::DiscordWebhook;
use crate::etwin::channel::{
  ChannelBorgbase, ChannelDomain, ChannelFallbackServer, ChannelPaths, ChannelPostgres, ChannelTarget, NodeEnv,
};
use crate::task::fs::{EnsureDir, EnsureDirError, EnsureDirSymlink, EnsureFile};
use crate::task::git::GitCheckout;
use crate::task::nginx::{NginxAvailableSite, NginxEnableSite};
use crate::task::postgres::PostgresUrl;
use crate::task::systemd::{
  EnsureSystemdServiceConfig, EnsureSystemdSliceConfig, SystemdSliceConfig, SystemdState, SystemdUnit,
};
use crate::task::witness::{FsWitness, FsWitnessError};
use crate::task::yarn::YarnEnv;
use crate::task::{exec_as, AsyncFn, ExecAsError, NamedTask, TaskName, TaskRef, TaskResult, TaskSuccess};
use crate::utils::display_error_chain::DisplayErrorChainExt;
use async_trait::async_trait;
use asys::common::fs::{FsHost, ReadFileError};
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata, ResolveError};
use asys::linux::user::{Gid, GroupRef, LinuxUser, LinuxUserHost, Uid, UserRef};
use asys::local_linux::LocalLinux;
use asys::{Command, ExecHost};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::path::PathBuf;
use url::Url;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgAppTarget<S: AsRef<str> = String> {
  pub name: S,
  pub user_name: S,
  pub group_name: S,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgApp<S: AsRef<str> = String> {
  name: S,
  user_name: S,
  group_name: S,
  uid: Uid,
  gid: Gid,
  home: PathBuf,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgChannelTarget<S: AsRef<str> = String> {
  pub inner: ChannelTarget<S>,
  pub vault: DinorpgVault,
  pub etwin_uri: Url,
  pub etwin_client_id: String,
  pub administrator: String,
  pub wss_port: u16,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Deserialize, Serialize)]
pub struct DinorpgVault {
  pub secret_key: String,
  pub etwin_client_secret: String,
  pub discord_webhook: Option<DiscordWebhook>,
  pub logs_webhook: Option<DiscordWebhook>,
  pub salt: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgChannel<User: LinuxUser, S: AsRef<str> = String> {
  full_name: S,
  short_name: S,
  user: User,
  domain: ChannelDomain<S>,
  paths: ChannelPaths,
  main_port: u16,
  fallback_server: ChannelFallbackServer,
  postgres: ChannelPostgres,
  node: NodeEnv,
  yarn: YarnEnv,
  vault: DinorpgVault,
  borg: Option<ChannelBorgbase>,
  etwin_uri: Url,
  etwin_client_id: String,
  administrator: String,
  wss_port: u16,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgReleaseTarget<S: AsRef<str> = String> {
  pub channel: DinorpgChannelTarget,
  pub repository: S,
  pub git_ref: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct DinorpgRelease<User: LinuxUser, S: AsRef<str> = String> {
  release_dir: PathBuf,
  repository: S,
  repo_dir: PathBuf,
  channel: DinorpgChannel<User, S>,
  git_ref: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeployDinorpg<S: AsRef<str> = String> {
  pub pass: S,
  pub target: DinorpgReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployDinorpgError {
  #[error("failed to exec dinorpg deployment task")]
  Exec(#[source] ExecAsError),
  #[error("failed to deploy dinorpg release")]
  Deploy(#[source] DeployDinorpgAsRootError),
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for DeployDinorpg<S>
where
  H: ExecHost,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), DeployDinorpgError>;

  async fn run(&self, _host: &'h H) -> Self::Output {
    let input = DeployDinorpgAsRoot {
      target: self.target.clone(),
    };
    exec_as::<LocalLinux, DeployDinorpgAsRoot>(UserRef::ROOT, Some(self.pass.as_ref()), &input)
      .await
      .map_err(DeployDinorpgError::Exec)?
      .map_err(DeployDinorpgError::Deploy)
  }
}

impl<'h, H: 'h + ExecHost> NamedTask<&'h H> for DeployDinorpg {
  const NAME: TaskName = TaskName::new("dinorpg::DeployDinorpg");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeployDinorpgAsRoot {
  target: DinorpgReleaseTarget,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DeployDinorpgAsRootError {
  #[error("failed to use witness file")]
  Witness(#[source] FsWitnessError),
  #[error("failed to run task `DinorpgReleaseTarget`")]
  Task(#[source] DinorpgReleaseTargetError),
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DeployDinorpgAsRoot
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<(), DeployDinorpgAsRootError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let paths: ChannelPaths = self.target.channel.inner.paths();
    let witness = paths.home.join("dinorpg_release.witness");
    let mut changed = false;
    let res = FsWitness::new(witness, TaskRef(&self.target))
      .run(host)
      .await
      .map_err(DeployDinorpgAsRootError::Witness)?;
    changed = res.changed || changed;
    let res = res.output.map_err(DeployDinorpgAsRootError::Task)?;
    changed = res.changed || changed;
    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for DeployDinorpgAsRoot
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync + Serialize + DeserializeOwned,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("dinorpg::DeployDinorpgAsRoot");
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
pub enum DinorpgReleaseTargetError {
  #[error("failed to ensure release directory")]
  ReleaseDir(#[source] EnsureDirError),
  #[error("failed to execute build task")]
  ExecBuild(#[source] ExecAsError),
  #[error("unexpected error: {0}")]
  Other(String),
}

impl From<anyhow::Error> for DinorpgReleaseTargetError {
  fn from(value: anyhow::Error) -> Self {
    Self::Other(value.display_chain().to_string())
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DinorpgReleaseTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::User: Debug + Clone + Send + Sync,
  H::Group: Debug + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<DinorpgRelease<H::User>, DinorpgReleaseTargetError>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let res = self.channel.run(host).await?;
    let channel: DinorpgChannel<H::User> = res.output;
    let mut changed = res.changed;

    let release_dir = channel.paths.releases.join(&self.git_ref);

    eprintln!("Create directory: {}", release_dir.display());
    changed = EnsureDir::new(release_dir.clone())
      .owner(UserRef::Id(channel.user.uid()))
      .group(GroupRef::Id(channel.user.gid()))
      .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await
      .map_err(DinorpgReleaseTargetError::ReleaseDir)?
      .changed
      || changed;

    let witness = release_dir.join("ok");

    let witness_exists = match host.read_file(witness.as_path()) {
      Ok(_) => true,
      Err(ReadFileError::NotFound) => false,
      Err(e) => {
        panic!("failed to check witness file: {e:?}")
      }
    };

    let repo_dir = release_dir.join("repo");

    let release = DinorpgRelease {
      release_dir,
      repository: self.repository.clone(),
      repo_dir: repo_dir.clone(),
      channel: channel.clone(),
      git_ref: self.git_ref.clone(),
    };

    if witness_exists {
      eprintln!("bailing-out, already deployed");
      return Ok(TaskSuccess {
        changed,
        output: release,
      });
    }

    let br = BuildDinorpgRelease {
      repo_dir: repo_dir.clone(),
      remote: self.repository.clone(),
      git_ref: self.git_ref.clone(),
      backend_config_env: config::BackendConfigEnv::from_channel(&channel, self.git_ref.clone()),
      frontend_config: config::FrontendConfig::from_channel_and_commit(&channel, self.git_ref.clone()),
      node: channel.node.clone(),
      yarn: channel.yarn.clone(),
    };

    changed = exec_as::<LocalLinux, _>(UserRef::Id(channel.user.uid()), None, &br)
      .await
      .map_err(DinorpgReleaseTargetError::ExecBuild)?
      .unwrap()
      .changed
      || changed;

    changed = (UpdateActiveRelease {
      release: release.clone(),
    })
    .run(host)
    .await?
    .changed
      || changed;

    changed = EnsureFile::new(witness).content([]).run(host).await.unwrap().changed || changed;

    Ok(TaskSuccess {
      changed,
      output: release,
    })
  }
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for DinorpgChannelTarget
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  H::Group: Debug + Send + Sync,
  H::User: Debug + Clone + Send + Sync,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = TaskResult<DinorpgChannel<H::User>, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    eprintln!("Dinorpg: Create channel");
    let res = self.inner.run(host).await?;

    eprintln!("Dinorpg: Channel ready");
    let output = DinorpgChannel {
      full_name: res.output.full_name,
      short_name: res.output.short_name,
      user: res.output.user,
      domain: res.output.domain.expect("Expected associated domain name"),
      paths: res.output.paths,
      main_port: res.output.main_port.expect("Expected main port"),
      fallback_server: res.output.fallback_server.expect("Expected fallback server"),
      postgres: res.output.postgres.expect("Expected Postgres database"),
      node: res.output.node.expect("Expected Node environment"),
      yarn: res.output.yarn.expect("Expected Yarn environment"),
      vault: self.vault.clone(),
      borg: res.output.borg,
      etwin_uri: self.etwin_uri.clone(),
      etwin_client_id: self.etwin_client_id.clone(),
      administrator: self.administrator.clone(),
      wss_port: self.wss_port,
    };

    Ok(TaskSuccess {
      changed: res.changed,
      output,
    })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct BuildDinorpgRelease {
  repo_dir: PathBuf,
  remote: String,
  git_ref: String,
  node: NodeEnv,
  yarn: YarnEnv,
  backend_config_env: config::BackendConfigEnv,
  frontend_config: config::FrontendConfig,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for BuildDinorpgRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed: bool;
    let gc = GitCheckout {
      path: self.repo_dir.clone(),
      remote: self.remote.clone(),
      r#ref: self.git_ref.clone(),
    };

    eprintln!("Checkout repository: {:?} {:?}", self.remote, self.git_ref);
    let res = gc.run(host).await.unwrap();
    changed = res.changed;

    let backend_dir = self.repo_dir.join("ed-be");
    let frontend_dir = self.repo_dir.join("ed-ui");

    eprintln!("Install Node dependencies");
    let cmd = Command::new("yarn")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .arg("install")
      .arg("--immutable")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    eprintln!("Configure the frontend build");
    EnsureFile::new(frontend_dir.join(".env.production"))
      .content(self.frontend_config.to_string())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap();

    eprintln!("Write .env backend config");
    changed = EnsureFile::new(backend_dir.join(".env"))
      .content(self.backend_config_env.to_string())
      // .owner(app.uid)
      // .group(app.gid)
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await
      .unwrap()
      .changed
      || changed;

    eprintln!("Build the project");
    let cmd = Command::new("yarn")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .arg("run")
      .arg("build")
      .env("NODE_ENV", "production")
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H: 'h + ExecHost + LinuxUserHost + LinuxFsHost> NamedTask<&'h H> for BuildDinorpgRelease
where
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("dinorpg::BuildDinorpgRelease");
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct UpdateActiveRelease<User: LinuxUser, S: AsRef<str> = String> {
  release: DinorpgRelease<User, S>,
}

#[async_trait]
impl<'h, H, S> AsyncFn<&'h H> for UpdateActiveRelease<H::User, S>
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
  S: AsRef<str> + Send + Sync,
{
  type Output = TaskResult<(), anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let uid = self.release.channel.user.uid();
    let gid = self.release.channel.user.gid();

    let service_name = format!("{}.service", self.release.channel.full_name.as_ref());

    let mut changed = false;
    let active_release_link = &self.release.channel.paths.active_release_link;
    let previous_release_link = &self.release.channel.paths.previous_release_link;
    let old_release_dir = {
      match host.resolve(active_release_link) {
        Ok(old) => Some(old),
        Err(ResolveError::NotFound) => None,
        Err(e) => return Err(e.into()),
      }
    };
    let new_release_dir = host.resolve(&self.release.release_dir)?;

    if let Some(old_release_dir) = old_release_dir {
      if old_release_dir != new_release_dir {
        eprintln!("Start to disable old release");
        // There is already an active release but it does not match the target
        // release: stop the old version before proceeding to free the channel
        // resources (database, port)
        EnsureDirSymlink::new(previous_release_link.clone())
          .points_to(old_release_dir.clone())
          .owner(uid)
          .group(gid)
          .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
          .run(host)
          .await?;

        SystemdUnit::new(&service_name)
          .state(SystemdState::Inactive)
          .run(host)
          .await?;
        eprintln!("Old release disabled");

        // if let Some(borg) = self.release.channel.borg.as_ref() {
        //   eprintln!("Creating archive before release");
        //   let backup_dir = self.release.channel.paths.home.join("backup");
        //   EnsureDir::new(backup_dir.clone()).run(host).await?;
        //   let example_file = backup_dir.join("etwin.pgdump");
        //   EnsureFile::new(example_file.clone()).content("Hey").run(host).await?;
        //
        //   changed = exec_as::<LocalLinux, _>(
        //     UserRef::Id(uid),
        //     None,
        //     &CreateBorgArchive::new_from_time(
        //       borg.password.clone(),
        //       borg.ssh_priv_key.clone(),
        //       borg.repo.clone(),
        //       backup_dir,
        //     ),
        //   )
        //   .await?
        //   .unwrap()
        //   .changed
        //     || changed;
        // } else {
        //   eprintln!("Skipping pre-release archive: no borg config");
        // }

        changed = true;
      }
    }

    // TODO: Trigger backup

    eprintln!("UpgradingDatabase");
    changed = exec_as::<LocalLinux, _>(
      UserRef::Id(uid),
      None,
      &UpgradeDinorpgDatabase {
        node: self.release.channel.node.clone(),
        yarn: self.release.channel.yarn.clone(),
        repo_dir: self.release.repo_dir.clone(),
        postgres: self.release.channel.postgres.clone(),
      },
    )
    .await?
    .unwrap()
    .changed
      || changed;

    if let Some(_borg) = self.release.channel.borg.as_ref() {
      eprintln!("WARNING: backups are not supported for DinoRPG");
    }

    eprintln!("Generating Systemd slice");
    {
      use crate::task::systemd::config::{ResourceControl, Slice, Unit};
      let slice = EnsureSystemdSliceConfig {
        name: self.release.channel.full_name.as_ref().to_string(),
        config: SystemdSliceConfig {
          unit: Some(Unit {
            description: Some(format!("Slice for {}", self.release.channel.full_name.as_ref())),
            ..Unit::default()
          }),
          slice: Some(Slice {
            resource_control: Some(ResourceControl {
              memory_high: None,
              memory_max: Some(String::from("4G")),
              cpu_quota: Some(String::from("200%")),
              ..ResourceControl::default()
            }),
            ..Slice::default()
          }),
          ..SystemdSliceConfig::default()
        },
      };
      changed = slice.run(host).await?.changed || changed;
    };

    eprintln!("Generating Systemd unit");
    {
      use crate::task::systemd::config::{
        CommandLine, Config, Exec, Install, ResourceControl, Service, ServiceType, Unit,
      };
      let description = format!(
        "DinoRPG ({channel_name})",
        channel_name = self.release.channel.full_name.as_ref()
      );
      let postgres = format!("{}.service", self.release.channel.postgres.cluster.service);
      let node_exe_str = self.release.channel.node.exe.display().to_string();
      let working_dir_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo/ed-be")
        .display()
        .to_string();
      let script_str = self
        .release
        .channel
        .paths
        .home
        .join("active/repo/ed-be/dist/main.js")
        .display()
        .to_string();
      let exec_start = format!("{node_exe_str} {script_str}");
      let path_env = format!("PATH={}:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/usr/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin", self.release.channel.node.bin_dir.display());
      let port_env = format!("PORT={}", self.release.channel.main_port);
      let pid_file_str = self
        .release
        .channel
        .paths
        .home
        .join(".local/var/run/dinorpg.pid")
        .display()
        .to_string();
      let slice_str = format!("{}.slice", self.release.channel.full_name.as_ref());

      let service_config = Config {
        unit: Some(Unit {
          description: Some(description.as_str()),
          after: vec!["network.target"],
          requires: vec![&postgres],
          ..Unit::default()
        }),
        service: Some(Service {
          r#type: Some(ServiceType::Exec),
          restart: Some("on-failure"),
          exec_start: Some(CommandLine {
            line: exec_start.as_str(),
          }),
          pid_file: Some(&pid_file_str),
          exec: Exec {
            user: Some(self.release.channel.user.name()),
            working_directory: Some(working_dir_str.as_str()),
            limit_no_file: Some("infinity"),
            limit_nproc: Some("infinity"),
            limit_core: Some("infinity"),
            environment: vec![&path_env, "NODE_ENV=production", &port_env],
            ..Exec::default()
          },
          resource_control: ResourceControl {
            slice: Some(&slice_str),
            ..ResourceControl::default()
          },
          ..Service::default()
        }),
        install: Some(Install {
          wanted_by: vec!["multi-user.target"],
          ..Install::default()
        }),
        ..Config::default()
      };
      eprintln!("Writing Systemd unit");
      changed = EnsureSystemdServiceConfig {
        name: self.release.channel.full_name.as_ref().to_string(),
        config: service_config,
      }
      .run(host)
      .await?
      .changed
        || changed;
    };

    eprintln!("Generating nginx config");
    let nginx_config = get_nginx_config(&self.release.channel);

    eprintln!("Marking release as active");
    EnsureDirSymlink::new(active_release_link.clone())
      .points_to(new_release_dir.clone())
      .owner(uid)
      .group(gid)
      .mode(FileMode::ALL_READ | FileMode::ALL_EXEC)
      .run(host)
      .await?;

    eprintln!("Starting service");
    changed = SystemdUnit::new(&service_name)
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Writing nginx config");
    NginxAvailableSite::new(self.release.channel.full_name.as_ref(), nginx_config)
      .run(host)
      .await?;

    eprintln!("Enabling nginx config");
    NginxEnableSite::new(self.release.channel.full_name.as_ref())
      .run(host)
      .await?;

    Ok(TaskSuccess { changed, output: () })
  }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct UpgradeDinorpgDatabase {
  node: NodeEnv,
  yarn: YarnEnv,
  repo_dir: PathBuf,
  postgres: ChannelPostgres,
}

#[async_trait]
impl<'h, H> AsyncFn<&'h H> for UpgradeDinorpgDatabase
where
  H: ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  type Output = Result<TaskSuccess<()>, ()>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let changed = false;

    let database_url = PostgresUrl::new(
      &self.postgres.host,
      self.postgres.port,
      &self.postgres.name,
      &self.postgres.admin.name,
      &self.postgres.admin.password,
      None,
      None,
    )
    .expect("failed to build admin Postgres URL");

    let cmd = Command::new("yarn")
      .arg("run")
      .arg("migration")
      .prepend_path(self.node.bin_dir.display().to_string())
      .prepend_path(self.yarn.bin_dir.display().to_string())
      .env("NODE_ENV", "production")
      .env("DATABASE_URL", database_url.to_string())
      .current_dir(self.repo_dir.to_str().unwrap());
    host.exec(&cmd).unwrap();

    Ok(TaskSuccess { changed, output: () })
  }
}

impl<'h, H> NamedTask<&'h H> for UpgradeDinorpgDatabase
where
  H: 'h + ExecHost + LinuxUserHost + LinuxFsHost,
  <H as FsHost>::Metadata: LinuxMetadata,
{
  const NAME: TaskName = TaskName::new("dinorpg::UpgradeDinorpgDatabase");
}

mod config {
  use crate::discord_webhook::DiscordWebhook;
  use crate::etwin::dinorpg::DinorpgChannel;
  use crate::task::postgres::PostgresUrl;
  use asys::linux::user::LinuxUser;
  use serde::{Deserialize, Serialize};
  use std::fmt;
  use url::Url;

  #[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
  pub struct BackendConfigEnv<S: AsRef<str> = String> {
    // Database
    pub database_url: PostgresUrl,
    // Server
    pub self_url: S,
    pub port: u16,
    // Etwin
    pub etwin_url: Url,
    pub etwin_client_id: S,
    pub etwin_client_secret: S,
    pub release_commit: S,
    pub release_channel_short: S,
    pub release_channel: S,
    pub discord_webhook: Option<DiscordWebhook<S>>,
    pub logs_webhook: Option<DiscordWebhook<S>>,
    pub salt: S,
    pub jwt_secret_key: S,
    pub jwt_expiration: u32,
    pub administrator: S,
    pub wss_port: u16,
  }

  impl BackendConfigEnv {
    pub fn from_channel(channel: &DinorpgChannel<impl LinuxUser>, git_ref: String) -> Self {
      const SECONDS_PER_DAY: u32 = 86400;
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let self_url = format!("{}://{}", protocol, channel.domain.main.as_str());
      Self {
        database_url: PostgresUrl::new(
          channel.postgres.host.as_str(),
          channel.postgres.port,
          channel.postgres.name.as_str(),
          channel.postgres.main.name.as_str(),
          channel.postgres.main.password.as_str(),
          None,
          None,
        )
        .expect("failed to build postgres URL"),
        self_url,
        port: channel.main_port,
        wss_port: channel.wss_port,
        etwin_url: channel.etwin_uri.clone(),
        etwin_client_id: channel.etwin_client_id.clone(),
        etwin_client_secret: channel.vault.etwin_client_secret.clone(),
        release_commit: git_ref,
        release_channel_short: channel.short_name.clone(),
        release_channel: channel.full_name.clone(),
        discord_webhook: channel.vault.discord_webhook.clone(),
        logs_webhook: channel.vault.logs_webhook.clone(),
        salt: channel.vault.salt.clone(),
        jwt_secret_key: channel.vault.secret_key.clone(),
        jwt_expiration: 30 * SECONDS_PER_DAY,
        administrator: channel.administrator.clone(),
      }
    }
  }

  impl<S: AsRef<str>> fmt::Display for BackendConfigEnv<S> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "DATABASE_URL={}", &self.database_url)?;
      writeln!(f, "PORT={}", self.port)?;
      writeln!(f, "WSS_PORT={}", self.wss_port)?;
      writeln!(f, "SELF_URL={}", self.self_url.as_ref())?;
      writeln!(f, "ETERNALTWIN_URL={}", self.etwin_url.as_str())?;
      writeln!(f, "ETWIN_URL={}", self.etwin_url.as_str())?;
      writeln!(f, "ETERNALTWIN_CLIENT_REF={}", self.etwin_client_id.as_ref())?;
      writeln!(f, "ETWIN_CLIENT_ID={}", self.etwin_client_id.as_ref())?;
      writeln!(f, "ETERNALTWIN_SECRET={}", self.etwin_client_secret.as_ref())?;
      writeln!(f, "ETWIN_CLIENT_SECRET={}", self.etwin_client_secret.as_ref())?;
      writeln!(f, "ETERNALTWIN_APP=dinorpg")?;
      writeln!(f, "ETERNALTWIN_CHANNEL={}", self.release_channel_short.as_ref())?;
      writeln!(f, "DATABASE_URL={}", &self.database_url)?;
      writeln!(f, "RELEASE_COMMIT={}", self.release_commit.as_ref())?;
      writeln!(f, "RELEASE_CHANNEL={}", self.release_channel.as_ref())?;
      if let Some(discord_webhook) = self.discord_webhook.as_ref() {
        writeln!(f, "DISCORD_WEBHOOK_ID={}", discord_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_WEBHOOK_TOKEN={}", discord_webhook.token.as_ref())?;
      }
      if let Some(logs_webhook) = self.logs_webhook.as_ref() {
        writeln!(f, "DISCORD_LOGS_WEBHOOK_ID={}", logs_webhook.id.as_ref())?;
        writeln!(f, "DISCORD_LOGS_WEBHOOK_TOKEN={}", logs_webhook.token.as_ref())?;
      }
      writeln!(f, "SALT={}", self.salt.as_ref())?;
      writeln!(f, "JWT_SECRET_KEY={}", self.jwt_secret_key.as_ref())?;
      writeln!(f, "JWT_EXPIRATION={}", self.jwt_expiration)?;
      writeln!(f, "ADMIN={}", self.administrator.as_ref())?;
      Ok(())
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct DbConfig {
    pub host: String,
    #[serde(rename = "dbName")]
    pub db_name: String,
    pub user: String,
    pub password: String,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct JwtConfig {
    #[serde(rename = "secretKey")]
    pub jwt_secret_key: String,
    pub jwt_expiration: u32,
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
  pub struct FrontendConfig {
    pub channel: String,
    pub commit: String,
    pub api_url: Url,
  }

  impl FrontendConfig {
    pub fn from_channel_and_commit(channel: &DinorpgChannel<impl LinuxUser>, commit: String) -> Self {
      let protocol = if channel.domain.cert.is_some() { "https" } else { "http" };
      let uri = format!("{}://{}", protocol, channel.domain.main.as_str());
      Self {
        channel: channel.full_name.clone(),
        commit,
        api_url: Url::parse(&uri).unwrap(),
      }
    }
  }

  impl fmt::Display for FrontendConfig {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
      writeln!(f, "VITE_API_URL={}", self.api_url)?;
      writeln!(f, "VITE_API_RELEASE_COMMIT={}", self.commit)?;
      writeln!(f, "VITE_API_RELEASE_CHANNEL={}", self.channel)?;
      writeln!(f, "NODE_ENV=production")?;
      Ok(())
    }
  }
}

pub fn get_nginx_config<S: AsRef<str>>(channel: &DinorpgChannel<impl LinuxUser, S>) -> String {
  let upstream_name = channel.full_name.as_ref().to_string().replace('.', "_");
  let access_log = channel.paths.logs.join("main.nginx.access.log");
  let error_log = channel.paths.logs.join("main.nginx.error.log");
  let static_dir = channel.paths.home.join("active/repo/ed-ui/dist");

  let fallback_server = format!(
    r#"# Fallback server
server {{
  # HTTP port
  listen {fallback_port};
  listen [::]:{fallback_port};

  # Hide nginx version in `Server` header.
  server_tokens off;
  add_header Referrer-Policy same-origin;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";

  index index.html;

  root {fallback_dir};
}}
"#,
    fallback_port = channel.fallback_server.port,
    fallback_dir = channel.fallback_server.dir.to_str().unwrap(),
  );

  let upstream_server = format!(
    r#"# Define target of the reverse-proxy
upstream {upstream_name} {{
  server [::1]:{port};
  server [::1]:{fallback_port} backup;
  keepalive 8;
}}

upstream {upstream_name}_ws {{
  server [::1]:{wss_port};
}}
"#,
    upstream_name = upstream_name,
    port = channel.main_port,
    fallback_port = channel.fallback_server.port,
    wss_port = channel.wss_port
  );

  let main_directives = format!(
    r#"  # Space-separated hostnames (wildcard * is accepted)
  server_name {domain};

  access_log {access_log};
  error_log {error_log};
  client_max_body_size 100M;

  root {static_dir};

  location ~* ^/(?:api|api-docs)(?:/|$) {{
    # Upstream reference
    proxy_pass http://{upstream_name};

    # Configuration of the proxy
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
    proxy_redirect off;
  }}

  location / {{
    try_files $uri /index.html;
    gzip_static on;
  }}

  location ~* ^/ws {{
    proxy_pass http://{upstream_name}_ws;
    proxy_http_version 1.1;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_set_header X-NginX-Proxy true;
    proxy_cache_bypass $http_upgrade;
  }}
"#,
    domain = channel.domain.main.as_ref(),
    access_log = access_log.to_str().unwrap(),
    error_log = error_log.to_str().unwrap(),
    static_dir = static_dir.to_str().unwrap(),
    upstream_name = upstream_name,
  );

  match channel.domain.cert.as_ref() {
    Some(https) => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTPS
server {{
  # HTTPS port
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  # # HSTS (ngx_http_headers_module is required) (63072000 seconds)
  # add_header Strict-Transport-Security "max-age=63072000" always;

  {main_directives}
}}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;
  server_name {domain};
  return 301 https://$host$request_uri;
}}

# www.
server {{
  listen 80;
  listen 443 ssl;
  listen [::]:80;
  listen [::]:443 ssl;

  server_name www.{domain};

  ssl_certificate {cert};
  ssl_certificate_key {cert_key};

  return 307 https://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
      cert = https.fullchain_cert_path.to_str().unwrap(),
      cert_key = https.cert_key_path.to_str().unwrap(),
    ),
    None => format!(
      r#"# This configuration sets a reverse proxy for a specific domain

{fallback_server}

{upstream_server}

# HTTP
server {{
  # HTTP port
  listen 80;
  listen [::]:80;

  {main_directives}
}}

# www.
server {{
  listen 80;
  listen [::]:80;

  server_name www.{domain};

  return 307 http://{domain}$request_uri;
}}

"#,
      fallback_server = fallback_server,
      upstream_server = upstream_server,
      main_directives = main_directives,
      domain = channel.domain.main.as_ref(),
    ),
  }
}
