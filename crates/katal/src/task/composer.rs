use indexmap::IndexMap;
use serde::{Deserialize, Serialize};

/// Content of a `content.json` file
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ComposerJson {
  pub name: Option<String>,
  pub description: Option<String>,
  pub project: Option<String>,
  #[serde(default)]
  pub require: IndexMap<String, String>,
  #[serde(default, rename = "require-dev")]
  pub require_dev: IndexMap<String, String>,
  #[serde(default)]
  pub scripts: IndexMap<String, ComposerScript>,
}

/// Value of the `scripts` field
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ComposerScript {
  /// A single command to run
  Simple(String),
  /// A list of commands to run sequentially
  List(Vec<String>),
  /// A sub-group of scripts
  Group(IndexMap<String, ComposerScript>),
}
