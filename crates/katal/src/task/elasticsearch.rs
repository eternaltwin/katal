use crate::task::fs::{EnsureDir, EnsureFile};
use crate::task::systemd::{SystemdState, SystemdUnit};
use crate::task::{AsyncFn, TaskResult, TaskSuccess};
use async_trait::async_trait;
use asys::common::fs::FsHost;
use asys::linux::fs::{FileMode, LinuxFsHost, LinuxMetadata};
use asys::linux::user::{LinuxUser, LinuxUserHost, Uid};
use asys::{Command, ExecHost};
use std::fmt;
use std::path::PathBuf;

pub const ELASTICSEARCH_JVM_OPTIONS: &str = include_str!("../../files/elasticsearch/jvm.options");
pub const LOG4J2_PROPERTIES: &str = include_str!("../../files/elasticsearch/log4j2.properties");

pub struct ElascticsearchClusterTarget {
  pub name: String,
  pub port: u16,
}

pub struct ElascticsearchCluster {
  pub name: String,
  pub port: u16,
  pub service: String,
}

#[async_trait]
impl<'h, H: ExecHost + LinuxUserHost + LinuxFsHost> AsyncFn<&'h H> for ElascticsearchClusterTarget
where
  <H as FsHost>::Metadata: LinuxMetadata,
  H::User: Send + Sync,
{
  type Output = TaskResult<ElascticsearchCluster, anyhow::Error>;

  async fn run(&self, host: &'h H) -> Self::Output {
    let mut changed = false;
    // TODO: Install from AUR if missing
    // changed = EnsurePacmanPackages::new()
    //   .present("elasticsearch")
    //   .run(host)
    //   .await?
    //   .changed
    //   || changed;

    let elasticsearch_user = host.user_from_name("elasticsearch")?;

    let cluster_config_dirs = PathBuf::from("/etc/elasticsearch.clusters");
    let cluster_config_dir = cluster_config_dirs.join(self.name.as_str());
    let cluster_data_dirs = PathBuf::from("/var/lib/elasticsearch.clusters");
    let cluster_data_dir = cluster_data_dirs.join(self.name.as_str());
    let cluster_logs_dirs = PathBuf::from("/var/log/elasticsearch.clusters");
    let cluster_logs_dir = cluster_logs_dirs.join(self.name.as_str());

    let dirs = [
      &cluster_config_dirs,
      &cluster_config_dir,
      &cluster_data_dirs,
      &cluster_data_dir,
      &cluster_logs_dirs,
      &cluster_logs_dir,
    ];
    for d in dirs {
      eprintln!("Create directory: {}", d.display());
      changed = EnsureDir::new(d.clone())
        .owner(elasticsearch_user.uid().into())
        .group(elasticsearch_user.gid().into())
        .mode(FileMode::OWNER_ALL | FileMode::ALL_READ | FileMode::ALL_EXEC)
        .run(host)
        .await?
        .changed
        || changed;
    }

    let pid_file = PathBuf::from("/run/elasticsearch").join(format!("{}.pid", self.name.as_str()));

    let es_config = ElasticsearchConfig {
      cluster_name: self.name.clone(),
      data_dir: cluster_data_dir.clone(),
      logs_dir: cluster_logs_dir.clone(),
      http_port: self.port,
    };

    eprintln!("Write Elasticsearch config");
    changed = EnsureFile::new(cluster_config_dir.join("elasticsearch.yml"))
      .content(es_config.to_string())
      .owner(elasticsearch_user.uid())
      .group(elasticsearch_user.gid())
      .mode(FileMode::OWNER_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Write Elasticsearch JVM options");
    changed = EnsureFile::new(cluster_config_dir.join("jvm.options"))
      .content(ELASTICSEARCH_JVM_OPTIONS)
      .owner(elasticsearch_user.uid())
      .group(elasticsearch_user.gid())
      .mode(FileMode::OWNER_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Write Elasticsearch log4j2 properties");
    changed = EnsureFile::new(cluster_config_dir.join("log4j2.properties"))
      .content(LOG4J2_PROPERTIES)
      .owner(elasticsearch_user.uid())
      .group(elasticsearch_user.gid())
      .mode(FileMode::OWNER_READ)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Write Elasticsearch users");
    changed = EnsureFile::new(cluster_config_dir.join("users"))
      .content([])
      .owner(elasticsearch_user.uid())
      .group(elasticsearch_user.gid())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Write Elasticsearch roles");
    changed = EnsureFile::new(cluster_config_dir.join("users_roles"))
      .content([])
      .owner(elasticsearch_user.uid())
      .group(elasticsearch_user.gid())
      .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
      .run(host)
      .await?
      .changed
      || changed;

    eprintln!("Create keystore");
    {
      let keystore = cluster_config_dir.join("elasticsearch.keystore");
      if !keystore.exists() {
        let cmd = Command::new("elasticsearch-keystore")
          .arg("create")
          // .env("ES_PATH_HOME", cluster_config_dir.display().to_string())
          .env("ES_PATH_CONF", cluster_config_dir.display().to_string());
        host.exec(&cmd)?;
      }
      if keystore.exists() {
        changed = EnsureFile::new(keystore.clone())
          .owner(elasticsearch_user.uid())
          .group(elasticsearch_user.gid())
          .mode(FileMode::OWNER_READ | FileMode::OWNER_WRITE)
          .run(host)
          .await?
          .changed
          || changed;
      } else {
        panic!("Failed to create keyStore");
      }
    }

    eprintln!("Write Systemd unit");
    let systemd_unit = ElasticsearchService {
      name: self.name.clone(),
      config_dir: cluster_config_dir.clone(),
      pid_file,
    };
    let service_name = format!("elasticsearch.{}.service", self.name.as_str());
    changed = EnsureFile::new(PathBuf::from("/etc/systemd/system").join(service_name.clone()))
      .content(systemd_unit.to_string())
      .owner(Uid::ROOT)
      .mode(FileMode::ALL_READ)
      .run(host)
      .await?
      .changed
      || changed;

    changed = SystemdUnit::new(service_name.as_str())
      .enabled(true)
      .state(SystemdState::Active)
      .run(host)
      .await?
      .changed
      || changed;

    let output = ElascticsearchCluster {
      name: self.name.clone(),
      port: self.port,
      service: service_name.clone(),
    };
    Ok(TaskSuccess { changed, output })
  }
}

pub struct ElasticsearchConfig {
  cluster_name: String,
  data_dir: PathBuf,
  logs_dir: PathBuf,
  http_port: u16,
}

impl fmt::Display for ElasticsearchConfig {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    writeln!(f, "cluster.name: {}", self.cluster_name.as_str())?;
    writeln!(f, "node.name: main")?;
    writeln!(f, "path.data: {}", self.data_dir.display())?;
    writeln!(f, "path.logs: {}", self.logs_dir.display())?;
    writeln!(f, "network.host: \"::1\"")?;
    writeln!(f, "http.port: {}", self.http_port)?;
    writeln!(f, "discovery.type: single-node")?;
    // write!(f, "xpack.security.enabled: true")?;
    Ok(())
  }
}

struct ElasticsearchService {
  name: String,
  config_dir: PathBuf,
  pid_file: PathBuf,
}

impl fmt::Display for ElasticsearchService {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      r#"[Unit]
Description=Elasticsearch (cluster {name})
Documentation=http://www.elastic.co
Wants=network-online.target
After=network-online.target

[Service]
Type=notify
RuntimeDirectory=elasticsearch
PrivateTmp=true
Environment=ES_HOME=/usr/share/elasticsearch
Environment=ES_PATH_CONF={config_dir}
Environment=PID_DIR=/run/elasticsearch
Environment=ES_SD_NOTIFY=true
EnvironmentFile=-/etc/default/elasticsearch

WorkingDirectory=/usr/share/elasticsearch

User=elasticsearch
Group=elasticsearch

PermissionsStartOnly=true
ExecStartPre=/usr/share/elasticsearch/bin/elasticsearch-keystore upgrade

ExecStart=/usr/share/elasticsearch/bin/elasticsearch -p {pid_file}

# StandardOutput is configured to redirect to journalctl since
# some error messages may be logged in standard output before
# elasticsearch logging system is initialized. Elasticsearch
# stores its logs in /var/log/elasticsearch and does not use
# journalctl by default. If you also want to enable journalctl
# logging, you can simply remove the "quiet" option from ExecStart.
StandardOutput=journal
StandardError=inherit

# Specifies the maximum file descriptor number that can be opened by this process
LimitNOFILE=65535

# Specifies the maximum number of processes
LimitNPROC=4096

# Specifies the maximum size of virtual memory
LimitAS=infinity

# Specifies the maximum file size
LimitFSIZE=infinity

# Disable timeout logic and wait until process is stopped
TimeoutStopSec=0

# SIGTERM signal is used to stop the Java process
KillSignal=SIGTERM

# Send the signal only to the JVM rather than its control group
KillMode=process

# Java process is never killed
SendSIGKILL=no

# When a JVM receives a SIGTERM signal it exits with code 143
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
"#,
      name = self.name.as_str(),
      pid_file = self.pid_file.display(),
      config_dir = self.config_dir.display(),
    )
  }
}
